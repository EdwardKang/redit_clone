class CreateSubsLinks < ActiveRecord::Migration
  def change
    create_table :subs_links do |t|
      t.integer :sub_id
      t.integer :link_id

      t.timestamps
    end
  end
end
