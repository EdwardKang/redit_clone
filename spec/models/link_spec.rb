require 'spec_helper'

describe Link do
  before(:each) do
    moderator1 = FactoryGirl.build(:user)
    sub1 = FactoryGirl.build(:sub, moderator_id: moderator1.id)
    link1 = FactoryGirl.build(:link)
    link2 = FactoryGirl.build(:link)
    subslinks1 = FactoryGirl.build(:subs_link, link_id: link1.id, sub_id: sub1.id)
    subslinks2 = FactoryGirl.build(:subs_link, link_id: link2.id, sub_id: sub1.id)
  end

  describe "assocations" do
    it { should have_many(:subs_links) }
    it { should have_many(:subs) }
  end
end
