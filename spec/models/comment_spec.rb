require 'spec_helper'

describe Comment do
  before(:each) do
    moderator1 = FactoryGirl.build(:user)
    sub1 = FactoryGirl.build(:sub, moderator_id: moderator1.id)
    link1 = FactoryGirl.build(:link)
    link2 = FactoryGirl.build(:link)
    subslinks1 = FactoryGirl.build(:subs_link, link_id: link1.id, sub_id: sub1.id)
    subslinks2 = FactoryGirl.build(:subs_link, link_id: link2.id, sub_id: sub1.id)
    comment1 = FactoryGirl.build(:comment, parent_comment_id: nil, link_id: link1.id)
    comment2 = FactoryGirl.build(:comment, parent_comment_id: comment1.id, link_id: link1.id)
  end

  describe "Associations" do
    it { should have_many(:children_comments) }
    it { should belong_to(:parent_comment) }
  end
end
