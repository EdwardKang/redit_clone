require 'spec_helper'

describe User do
  it "should not show password" do
    expect(FactoryGirl.build(:user).password).should be_nil
  end

  it "should be invalid if same username" do
    user1 = User.create!(username: "Billy", password: "new_pass")
    user2 = User.new(username: "Billy", password: "new_pass")

    expect(user2).not_to be_valid
  end
end
