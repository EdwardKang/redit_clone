# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :user do
    username Faker::Name.first_name
    password Faker::Name.last_name
  end
end
