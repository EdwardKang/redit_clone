# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :link do
    url Faker::Internet.url
    title Faker::Name.first_name
    #text Faker::Name.first_name
  end
end
