class Link < ActiveRecord::Base
  attr_accessible :title, :url

  has_many(
    :subs_links,
    class_name: 'SubsLinks',
    foreign_key: :link_id,
    primary_key: :id
  )

  has_many :subs, through: :subs_links, source: :sub

  has_many(
    :comments,
    class_name: 'Comment',
    foreign_key: :link_id,
    primary_key: :id
  )

  def comments_by_parent_id
    comments_by_pid = {}
    comments_by_pid[nil] = []
    self.comments.each do |comment|
      if comment.parent_comment.nil?
        comments_by_pid[nil] << comment
      end
      comments_by_pid[comment.id] = comment.children_comments unless comment.children_comments.empty?
    end

    comments_by_pid
  end

end
