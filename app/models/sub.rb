class Sub < ActiveRecord::Base
  attr_accessible :name, :moderator_id

  belongs_to(
    :moderator,
    class_name: 'User',
    foreign_key: :moderator_id,
    primary_key: :id
  )

  has_many(
    :subs_links,
    class_name: 'SubsLinks',
    foreign_key: :sub_id,
    primary_key: :id
  )

  has_many :links, through: :subs_links, source: :link

end
