class User < ActiveRecord::Base
  attr_accessible :password, :token, :username, :password_digest
  attr_reader :password

  validates :username, presence: true
  validates :username, uniqueness: true

  has_many(
    :subs,
    class_name: 'Sub',
    foreign_key: :moderator_id,
    primary_key: :id
  )

  def password=(password)
    self.password_digest = BCrypt::Password.create(password)
  end

  def has_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def reset_token
    self.token = SecureRandom::urlsafe_base64(16)
    self.save!
    self.token
  end
end
